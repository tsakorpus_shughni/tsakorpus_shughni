# A shell script used to prepare the corpus files for indexing.
# Runs a convertor and puts the resulting JSON files to the appropriate folder.
START_TIME="$(date -u +%s)"
cd src_convertors
python3 eaf2json.py
echo "Source conversion done."
rm -rf ../corpus/shughni
mkdir -p ../corpus/shughni
mv corpus/json ../corpus/shughni
rm -rf corpus/json
END_TIME="$(date -u +%s)"
ELAPSED_TIME="$(($END_TIME-$START_TIME))"
echo "Corpus files prepared in $ELAPSED_TIME seconds, finishing now."